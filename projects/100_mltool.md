---
title: mltool
description: Machine Learning Toolkit
github: https://github.com/Alexander-Ignatyev/mltool
tags: mltool
---
Haskell Machine Learning Toolkit includes various methods of supervised learning: linear regression, logistic regression, SVN, neural networks, etc. as well as some methods of unsupervised methods: K-Means and PCA.
