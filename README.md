# www.aligus.net

## Build

* required to use `--recursive` to git clone because it contains git submodules


```bash
make  # to build Elm dependencies
stack build
stack exec site watch
```
